use spiral_rs::params::*;
use spiral_rs::util::*;

// Get the Spiral params for a database of 2^r 8-byte entries.  These
// params are taken from spiral's params_store.json file, but adjusted
// for 8-byte entries.

pub fn get_spiral_params(r: usize) -> Params {
    let json = match r {
        15 => {
            r#"{
      "n": 2,
      "nu_1": 6,
      "nu_2": 4,
      "p": 4,
      "q2_bits": 13,
      "t_gsw": 4,
      "t_conv": 4,
      "t_exp_left": 4,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 256
    }"#
        }
        16 => {
            r#"{
      "n": 2,
      "nu_1": 6,
      "nu_2": 4,
      "p": 4,
      "q2_bits": 13,
      "t_gsw": 4,
      "t_conv": 4,
      "t_exp_left": 4,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 512
    }"#
        }
        17 => {
            r#"{
      "n": 2,
      "nu_1": 6,
      "nu_2": 4,
      "p": 4,
      "q2_bits": 13,
      "t_gsw": 4,
      "t_conv": 4,
      "t_exp_left": 4,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 1024
    }"#
        }
        18 => {
            r#"{
      "n": 2,
      "nu_1": 6,
      "nu_2": 4,
      "p": 4,
      "q2_bits": 13,
      "t_gsw": 4,
      "t_conv": 4,
      "t_exp_left": 4,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 2048
    }"#
        }
        19 => {
            r#"{
      "n": 2,
      "nu_1": 7,
      "nu_2": 4,
      "p": 4,
      "q2_bits": 13,
      "t_gsw": 4,
      "t_conv": 32,
      "t_exp_left": 4,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 2048
    }"#
        }
        20 => {
            r#"{
      "n": 2,
      "nu_1": 7,
      "nu_2": 4,
      "p": 16,
      "q2_bits": 16,
      "t_gsw": 4,
      "t_conv": 4,
      "t_exp_left": 4,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 4096
    }"#
        }
        21 => {
            r#"{
      "n": 2,
      "nu_1": 7,
      "nu_2": 5,
      "p": 16,
      "q2_bits": 16,
      "t_gsw": 5,
      "t_conv": 4,
      "t_exp_left": 4,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 4096
    }"#
        }
        22 => {
            r#"{
      "n": 2,
      "nu_1": 8,
      "nu_2": 5,
      "p": 16,
      "q2_bits": 16,
      "t_gsw": 5,
      "t_conv": 4,
      "t_exp_left": 4,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 4096
    }"#
        }
        23 => {
            r#"{
      "n": 2,
      "nu_1": 8,
      "nu_2": 6,
      "p": 16,
      "q2_bits": 16,
      "t_gsw": 5,
      "t_conv": 4,
      "t_exp_left": 4,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 4096
    }"#
        }
        24 => {
            r#"{
      "n": 2,
      "nu_1": 8,
      "nu_2": 6,
      "p": 256,
      "q2_bits": 20,
      "t_gsw": 8,
      "t_conv": 4,
      "t_exp_left": 8,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 8192
    }"#
        }
        25 => {
            r#"{
      "n": 2,
      "nu_1": 9,
      "nu_2": 6,
      "p": 256,
      "q2_bits": 20,
      "t_gsw": 8,
      "t_conv": 4,
      "t_exp_left": 8,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 8192
    }"#
        }
        26 => {
            r#"{
      "n": 2,
      "nu_1": 9,
      "nu_2": 7,
      "p": 256,
      "q2_bits": 20,
      "t_gsw": 8,
      "t_conv": 4,
      "t_exp_left": 8,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 8192
    }"#
        }
        27 => {
            r#"{
      "n": 2,
      "nu_1": 9,
      "nu_2": 8,
      "p": 256,
      "q2_bits": 20,
      "t_gsw": 8,
      "t_conv": 4,
      "t_exp_left": 8,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 8192
    }"#
        }
        28 => {
            r#"{
      "n": 2,
      "nu_1": 10,
      "nu_2": 8,
      "p": 256,
      "q2_bits": 20,
      "t_gsw": 8,
      "t_conv": 4,
      "t_exp_left": 16,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 8192
    }"#
        }
        29 => {
            r#"{
      "n": 2,
      "nu_1": 10,
      "nu_2": 9,
      "p": 256,
      "q2_bits": 23,
      "t_gsw": 9,
      "t_conv": 4,
      "t_exp_left": 16,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 8192
    }"#
        }
        30 => {
            r#"{
      "n": 4,
      "nu_1": 10,
      "nu_2": 8,
      "p": 256,
      "q2_bits": 20,
      "t_gsw": 8,
      "t_conv": 4,
      "t_exp_left": 16,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 32768
    }"#
        }
        31 => {
            r#"{
      "n": 8,
      "nu_1": 10,
      "nu_2": 8,
      "p": 256,
      "q2_bits": 20,
      "t_gsw": 8,
      "t_conv": 4,
      "t_exp_left": 16,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 65536
    }"#
        }
        32 => {
            r#"{
      "n": 8,
      "nu_1": 10,
      "nu_2": 9,
      "p": 256,
      "q2_bits": 20,
      "t_gsw": 10,
      "t_conv": 32,
      "t_exp_left": 16,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 65536
    }"#
        }
        33 => {
            r#"{
      "n": 8,
      "nu_1": 10,
      "nu_2": 10,
      "p": 256,
      "q2_bits": 20,
      "t_gsw": 10,
      "t_conv": 32,
      "t_exp_left": 16,
      "t_exp_right": 56,
      "instances": 1,
      "db_item_size": 65536
    }"#
        }
        _ => panic!(),
    };
    params_from_json(json)
}
